NAME	= Test_lib_no_cri

CC	= gcc

RM	= rm -f

SRC_DIR	= ./src

SRC_PATH = ./lib/epi/src

SRCS	=	$(SRC_PATH)/epi_putstr.c		\
		$(SRC_PATH)/epi_puterr.c		\
		$(SRC_PATH)/epi_getnbr_long.c		\
		$(SRC_PATH)/epi_strlen.c		\
		$(SRC_PATH)/epi_fusion_str.c		\
		$(SRC_PATH)/epi_compare_str.c		\
		$(SRC_PATH)/epi_clear_malloc.c		\
		$(SRC_PATH)/epi_count_words.c		\
		$(SRC_PATH)/epi_split_word.c		\
		$(SRC_PATH)/epi_wtoa.c			\
		$(SRC_PATH)/epi_putnbr.c		\
		$(SRC_PATH)/epi_itoa.c			\
		$(SRC_PATH)/epi_putfloat.c		\
		$(SRC_PATH)/epi_printf.c		\
		$(SRC_PATH)/epi_mprintf.c		\
		$(SRC_PATH)/epi_malloc.c		\
		$(SRC_PATH)/epi_nmatch.c		\
		$(SRC_PATH)/epi_re_malloc.c		\
		$(SRC_PATH)/epi_replace_charac.c	\
		$(SRC_PATH)/epi_putnbr_base.c		\
		$(SRC_PATH)/epi_putnbr_dec.c		\
		$(SRC_PATH)/epi_clone_army.c		\
		$(SRC_PATH)/epi_printf_flags.c		\
		$(SRC_PATH)/epi_does_start_with.c	\
		$(SRC_PATH)/epi_contain.c		\
		$(SRC_PATH)/epi_split_many.c		\
		$(SRC_PATH)/epi_dadd.c			\
		$(SRC_PATH)/epi_stradd.c		\
		$(SRC_PATH)/epi_putf.c			\
		$(SRC_PATH)/epi_free.c			\
		$(SRC_PATH)/epi_split_nb.c		\
		$(SRC_PATH)/epi_putf_flags.c		\
		$(SRC_PATH)/epi_split_light.c		\
		$(SRC_PATH)/epi_sort.c			\
		$(SRC_PATH)/epi_get_words.c		\
		$(SRC_PATH)/epi_base_to_base.c		\
		$(SRC_PATH)/epi_chained.c		\
		$(SRC_PATH)/epi_destruct_chained.c	\
		$(SRC_PATH)/epi_obj_basics.c		\
		$(SRC_PATH)/epi_obj_basics2.c		\
		$(SRC_PATH)/epi_obj_advanced.c		\
		$(SRC_PATH)/epi_get_args.c		\
		$(SRC_DIR)/tests.c			\
		$(SRC_DIR)/tests2.c			\


OBJS	= $(SRCS:.c=.o)

CFLAGS = -I ./include
CFLAGS += -W -Wall -Wextra -pedantic -g3

all: lib_compil $(NAME)

lib_compil:
	(cd ./lib/epi/ && make libepi_obj)
	(cd ./lib/epi/ && make clean)

$(NAME): $(OBJS)
	$(CC) $(OBJS) -o $(NAME)

clean:
	$(RM) $(OBJS)

fclean: clean
	(cd lib/epi && make fclean)
	$(RM) $(NAME)

re: fclean all
