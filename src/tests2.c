/*
** AZKALAAK PROJECT, 2018
** my graphical lib
** File description:
** creating texture
*/

#include "declaration.h"

void test8(void)
{
	epi_compare_str("", "hello");
	epi_compare_str("hello", "");
	epi_compare_str("hello", "hello");
}

void test7(void)
{
	char *argv[] = {
		"salut",
		"je",
		"--vais",
		"-bien"
	};
	epi_args_t *arguments = epi_get_args(4, argv);

	epi_printf("small arguments: %s\n", arguments->small_args);
	epi_printd(arguments->argv);
	epi_ddestroy(arguments->argv);
	epi_ddestroy(arguments->big_args);
	epi_free_many(arguments->small_args, arguments, 1);
	epi_printf("\n");
	epi_printf("\n%d\n%d\n%d\n%d\n", epi_contain_char("test", 'c'),
	epi_contain_char("test", 'e'), epi_contain_str("test", "eq"),
	epi_contain_str("test", "es"));
	test8();
}

void test6(void)
{
	object_t obj = new_obj("12", "string");
	long long *tmp;

	epi_printf("%s\n", obj->get_value(obj));
	obj->to_int(obj);
	tmp = obj->get_value(obj);
	epi_printf("%d\n", (int)tmp[0]);
	obj->print(obj);
	obj->to_string(obj);
	obj->print(obj);
	epi_putchar('\n');
	obj->destroy(obj);
	test7();
}

void test5(void)
{
	char *sol = epi_base_to_base("10", 10, 16, 0);
	chained_t *test = epi_create_chained(2);

	epi_add_chained(test, -1);
	epi_remove_chained(test, 2);
	epi_destruct_chained(test);
	epi_printf("%s\n", sol);
	epi_free(sol);
	test6();
}
