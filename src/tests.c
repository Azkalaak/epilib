/*
** AZKALAAK PROJECT, 2018
** tests lib
** File description:
** testing lib no cricri
*/

#include "declaration.h"

void test5(void);

void tests1(void)
{
	char *sol = NULL;

	epi_putnbr_err(-200);
	epi_printnbr_base(2, 2);
	epi_printnbr_base(10, 16);
	epi_putfloat(12.42);
	sol = epi_remalloc("j3 vais bi3n\n", -1);
	epi_replace_charac(sol, '3', 'e');
	epi_putstr(sol);
	epi_free(sol);
	epi_atoi("-12");
	epi_atoi("0");
	epi_atoi(NULL);
	epi_atoi("42test84");
	epi_printf("flag test: %s, %S, %d, %i, %b, %%, %#o, %#x, %#X, %o, %x, %X, %u, %c, %p\n",
	"test", "test\a\n", 12, 14, 2, 19, 0, 42, 8, 15, 10, -20, 'c', "test");
	sol = epi_putf("flag test: %s, %S, %d, %i, %b, %%, %#o, %#x, %#X, %o, %x, %X, %u, %c, %p",
	"test", "test\a\n", 12, 14, 2, 19, 0, 42, 8, 15, 10, -20, 'c', "test");
	epi_free(sol);
	test5();
}

void tests2(void)
{
	char *sol = NULL;
	char **strbis = NULL;

	epi_count_words("salut ceci est une phrase.", ' ');
	epi_count_words("hey hello happy holydays", 'h');
	epi_count_words("            salut ceci est une phrase     ", ' ');
	strbis = epi_split("         salut      ceci est un test\n     ", ' ');
	sol = epi_join(strbis, ' ');
	epi_start_with("salut, ceci est un test", "salut");
	epi_start_with("hello, je vais bien", "hello salut");
	epi_start_with("test", "nope, trop long");
	epi_start_with(NULL, NULL);
	epi_start_with("test", "tester est ma passion");
	epi_free(sol);
	epi_ddestroy(strbis);
	epi_end_with("salut, ceci est un test\n", "\n", 0);
	epi_end_with("salut, ceci est un test\n", "test", 0);
	epi_end_with("test", "salut, ceci est un test", 0);
	epi_end_with(NULL, NULL, 0);
	epi_count_words_arg("salut ceci est une phrase.", ' ');
	tests1();
}

void test3(void)
{
	char *sol = NULL;
	char **strbis = NULL;

	epi_count_words_arg("hey hello happy holydays", 'h');
	epi_count_words_arg("            salut ceci est une phrase     ", ' ');
	epi_count_words_arg("salut \"ça va\" ?\n", ' ');
	strbis = epi_split_arg("'hey '        salut 'ceci est' un test\n", ' ');
	epi_ddestroy(strbis);
	strbis = epi_split_nb("je suis un poneysss", 9, " ");
	epi_ddestroy(strbis);
	strbis = epi_split_many("hey, salut\t ceci \t est  un test\n\t", "\t ");
	epi_ddestroy(strbis);
	strbis = epi_split_many_arg("salut", "\t ");
	epi_ddestroy(strbis);
	sol = "salut, ceci est un test\n";
	epi_remove_charac(&sol, ' ');
	epi_free(sol);
	sol = epi_fusion_str_many("salut ", "je ", "vais ", "bien.\n", NULL);
	epi_printf("%s", sol);
	epi_free(sol);
	tests2();
}

void test4(void)
{
	char **strbis = NULL;

	strbis = epi_split("salut je vais bien.\n", ' ');
	epi_ddestroy(epi_remove_line(&strbis, "vais"));
	epi_ddestroy(epi_remove_line(&strbis, "bien.\n"));
	epi_printd(strbis);
	epi_ddestroy(strbis);
	strbis = epi_split("salut je vais bien.\n", ' ');
	epi_ddestroy(epi_remove_line_nb(&strbis, 1));
	epi_ddestroy(epi_remove_line_nb(&strbis, 1));
	epi_printd(strbis);
	epi_ddestroy(strbis);
	strbis = epi_split("J'AIME LES", ' ');
	strbis = epi_dadd(strbis, "PONEY");
	epi_ddestroy(strbis);
	strbis = epi_split("LES PONEY", ' ');
	strbis = epi_dadd_start(strbis, "J'AIME");
	epi_ddestroy(strbis);
	test3();
}

int main(void)
{
	int i = epi_putnbr_dec("A", 16);
	char *sol = "salut ca va";
	char *str[] = {"salut ", "je ", "vais ", "bien\n", NULL};
	char **strbis = epi_dre_malloc(str);

	epi_strlen("test");
	epi_strlen(NULL);
	epi_putstr("test\n");
	epi_putstr(NULL);
	epi_puterr("test\n");
	epi_puterr(NULL);
	epi_stradd(&sol, '?');
	epi_free(sol);
	epi_dprintf(1, "%d\n%d\n", i, -1);
	epi_printd(strbis);
	epi_printd(str);
	epi_ddestroy(strbis);
	epi_putnbr(200);
	epi_putnbr(-200);
	test4();
	return 0;
}
