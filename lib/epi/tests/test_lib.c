/*
** EPITECH PROJECT, 2018
** testing lib 2.0
** File description:
** the criterion testing file :D
*/

#define NORMAL_COLOR "\x1B[0m"
#define RED_COLOR "\x1B[31m"
#define GREEN_COLOR "\x1B[32m"
#define YELLOW_COLOR "\x1B[33m"
#define BLUE_COLOR "\x1B[34m"
#define MAGENTA_COLOR "\x1B[35m"
#define CYAN_COLOR "\x1B[36m"
#define WHITE_COLOR "\x1B[37m"
#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include <ctype.h>
#include "../internal.h"

Test(epi_strlen, all_tests)
{
	cr_expect(epi_strlen("test") == 4);
	cr_expect(epi_strlen(NULL) == 0);
}

Test(epi_putstr, all_tests)
{
	printf("\a");
	cr_redirect_stdout();
	epi_putstr("test\n");
	epi_putstr(NULL);
	cr_expect_stdout_eq_str("test\n(NULL)");
}

Test(epi_puterr, all_tests)
{
	printf("\a");
	cr_redirect_stderr();
	epi_puterr("test\n");
	epi_puterr(NULL);
	cr_expect_stderr_eq_str("test\n(NULL)\n");
}

Test(epi_stradd, all_tests)
{
	char *str = "salut ca va";

	epi_stradd(&str, '?');
	cr_expect_str_eq(str, "salut ca va?");
	free(str);
}

Test(epi_printd_and_dfunc, all_tests)
{
	char *str[] = {"salut ", "je ", "vais ", "bien\n", NULL};
	char **strbis = epi_dre_malloc(str);

	printf("\a");
	cr_redirect_stdout();
	epi_printd(strbis);
	epi_printd(str);
	epi_ddestroy(strbis);
	cr_expect_stdout_eq_str("salut je vais bien\nsalut je vais bien\n");
}

Test(epi_putnbr, all_tests)
{
	printf("\a");
	cr_redirect_stdout();
	epi_putnbr(200);
	epi_putnbr(0);
	epi_putnbr(-200);
	cr_expect_stdout_eq_str("2000-200");
}

Test(epi_putnbr_err, all_tests)
{
	printf("\a");
	cr_redirect_stderr();
	epi_putnbr_err(200);
	epi_putnbr_err(0);
	epi_putnbr_err(-200);
	cr_expect_stderr_eq_str("2000-200");
}

Test(epi_printnbr_base, all_tests)
{
	printf("\a");
	cr_redirect_stdout();
	epi_printnbr_base(2, 2);
	epi_printnbr_base(10, 16);
	cr_expect_stdout_eq_str("10a");
}

Test(epi_putfloat, all_tests)
{
	printf("\a");
	cr_redirect_stdout();
	epi_putfloat(12.42);
	cr_expect_stdout_eq_str("12.420000");
}

Test(epi_replace_charac_and_remalloc, all_tests)
{
	char *str = epi_remalloc("j3 vais bi3n\n", -1);
	char *strbis = epi_remalloc("\r", -1);

	printf("\a");
	cr_redirect_stdout();
	epi_replace_charac(str, '3', 'e');
	epi_remove_charac(&strbis, '\r');
	epi_putstr(str);
	cr_expect_stdout_eq_str("je vais bien\n");
	free(str);
}

Test(epi_atoi, all_tests)
{
	cr_expect(epi_atoi("-12") == -12);
	cr_expect(epi_atoi("0") == 0);
	cr_expect(epi_atoi(NULL) == 0);
	cr_expect(epi_atoi("42test84") == 42);
}

Test(epi_printf, all_tests)
{
	char *temp = epi_putnbr_base((long long)"test", 16);
	char *sol =epi_fusion_str("flag test: test, test\\007\\012, 12, 14, 10, %, 023, 0, 0X2A, 10, f, A, 4294967276, c, 0x", temp);

	free(temp);
	temp = sol;
	sol =epi_fusion_str(sol, "\n");
	free(temp);
	printf("\a");
	cr_redirect_stdout();
	epi_dprintf(1, "flag test: %s, %S, %d, %i, %b, %%, %#o, %#x, %#X, %o, %x, %X, %u, %c, %p\n",
	"test", "test\a\n", 12, 14, 2, 19, 0, 42, 8, 15, 10, -20, 'c', "test");
	cr_expect_stdout_eq_str(sol);
	free(sol);
}

Test(epi_putf, all_tests)
{
	char *temp = epi_putnbr_base((long long)"test", 16);
	char *pos =epi_fusion_str("flag test: test, test\\007\\012, 12, 14, 10, %, 023, 0, 0X2A, 10, f, A, 4294967276, c, 0x", temp);
	char *sol = epi_putf("flag test: %s, %S, %d, %i, %b, %%, %#o, %#x, %#X, %o, %x, %X, %u, %c, %p",
	"test", "test\a\n", 12, 14, 2, 19, 0, 42, 8, 15, 10, -20, 'c', "test");

	cr_expect_str_eq(sol, pos);
	free(sol);
	free(temp);
	free(pos);
}

Test(epi_count_words, all_tests)
{
	epi_dprintf(1, "%s%s%s%s\n%s%s%s%s\n", RED_COLOR, "[GLaDOS]$", NORMAL_COLOR, " delete Caroline.",
	GREEN_COLOR, "[Central Operator]$", NORMAL_COLOR, " Caroline Deleted.");
	cr_expect(epi_count_words("salut ceci est une phrase.", ' ') == 5);
	cr_expect(epi_count_words("hey hello happy holydays", 'h') == 4);
	cr_expect(epi_count_words("            salut ceci est une phrase     ", ' ') == 5);
}

Test(epi_split, all_tests)
{
	char **str = epi_split("            salut      ceci est un test\n     ", ' ');

	printf("\a");
	cr_redirect_stdout();
	epi_printd(str);
	epi_ddestroy(str);
	cr_expect_stdout_eq_str("salutceciestuntest\n");
}

Test(epi_join, all_tests)
{
	char **str = epi_split("        salut         ceci est un      test\n        ", ' ');
	char *sol = epi_join(str, ' ');

	printf("\a");
	cr_redirect_stdout();
	epi_printf(sol);
	free(sol);
	epi_ddestroy(str);
	cr_expect_stdout_eq_str("salut ceci est un test\n");
}

Test(does_start_with, all_tests)
{
	cr_expect(epi_start_with("salut, ceci est un test", "salut") == 5);
	cr_expect(epi_start_with("hello, je vais bien", "hello salut") == -1);
	cr_expect(epi_start_with("test", "nope, trop long") == -1);
	cr_expect(epi_start_with(NULL, NULL) == -1);
	cr_expect(epi_start_with("test", "tester est ma passion") == -1);
}

Test(does_end_with, all_tests)
{
	cr_expect(epi_end_with("salut, ceci est un test\n", "\n", 0) == 1);
	cr_expect(epi_end_with("salut, ceci est un test\n", "test", 0) == 0);
	cr_expect(epi_end_with("test", "salut, ceci est un test", 0) == 0);
	cr_expect(epi_end_with(NULL, NULL, 0) == 0);
}

Test(epi_count_words_arg, all_tests)
{
	cr_expect(epi_count_words_arg("salut ceci est une phrase.", ' ') == 5);
	cr_expect(epi_count_words_arg("hey hello happy holydays", 'h') == 4);
	cr_expect(epi_count_words_arg("            salut ceci est une phrase     ", ' ') == 5);
	cr_expect(epi_count_words_arg("salut \"ça va\" ?\n", ' ') == 3);
}

Test(epi_split_arg, all_tests)
{
	char **str = epi_split_arg("'hey '        salut 'ceci est' un test\n", ' ');

	printf("\a");
	cr_redirect_stdout();
	epi_printd(str);
	epi_ddestroy(str);
	cr_expect_stdout_eq_str("'hey 'salut'ceci est'untest\n");
}

Test(epi_split_nb, all_tests)
{
	char **str = epi_split_nb("je suis un poneysss", 9, " ");
	char *sol = epi_join(str, '\n');

	printf("\a");
	cr_redirect_stdout();
	epi_dprintf(1, "%s", sol);
	epi_ddestroy(str);
	free(sol);
	cr_expect_stdout_eq_str("je suis\nun\nponeysss");
}

Test(epi_split_many, all_tests)
{
	char **str = epi_split_many("hey, salut\t ceci \t est      un test\n\t", "\t ");

	printf("\a");
	cr_redirect_stdout();
	epi_printd(str);
	epi_ddestroy(str);
	cr_expect_stdout_eq_str("hey,salutceciestuntest\n");
}

Test(epi_split_many_arg, all_tests)
{
	char **str = epi_split_many_arg("salut", "\t ");

	printf("\a");
	cr_redirect_stdout();
	epi_printd(str);
	epi_ddestroy(str);
	cr_expect_stdout_eq_str("salut");
}

Test(epi_remove_charac, all_tests)
{
	char *str = "salut, ceci est un test\n";
	char *strbis = epi_remalloc("salut, ceci est un test\n", -1);

	printf("\a");
	cr_redirect_stdout();
	epi_remove_charac(&str, '\n');
	free(epi_remove_charac(&strbis, ' '));
	epi_printf(str);
	epi_printf(strbis);
	cr_expect_stdout_eq_str("salut, ceci est un testsalut,ceciestuntest\n");
}

Test(epi_fusion_str_many, all_tests)
{
	char *str = epi_fusion_str_many("salut ", "je ", "vais ", "bien.\n", NULL);

	cr_expect_str_eq(str, "salut je vais bien.\n");
	free(str);
	str = epi_fusion_str_many("salut", NULL);
	cr_expect_str_eq(str, "salut");
}

Test(epi_remove_line, all_tests)
{
	char **str = epi_split("salut je vais bien.\n", ' ');

	printf("\a");
	cr_redirect_stdout();
	epi_ddestroy(epi_remove_line(&str, "vais"));
	epi_ddestroy(epi_remove_line(&str, "bien.\n"));
	epi_printd(str);
	cr_expect_stdout_eq_str("salutje");
}

Test(epi_remove_line_nb, all_tests)
{
	char **str = epi_split("salut je vais bien.\n", ' ');

	printf("\a");
	cr_redirect_stdout();
	epi_ddestroy(epi_remove_line_nb(&str, 1));
	epi_ddestroy(epi_remove_line_nb(&str, 1));
	epi_printd(str);
	cr_expect_stdout_eq_str("salutbien.\n");
}

Test(epi_dadd, all_tests)
{
	char **str = epi_split("J'AIME LES", ' ');
	char **tmp = NULL;

	tmp = epi_dadd(str, "PONEY");
	printf("\a");
	cr_redirect_stdout();
	epi_printd(tmp);
	epi_ddestroy(tmp);
	cr_expect_stdout_eq_str("J'AIMELESPONEY");
}

Test(epi_dadd_start, all_tests)
{
	char **str = epi_split("LES PONEY", ' ');
	char **tmp = NULL;

	tmp = epi_dadd_start(str, "J'AIME");
	printf("\a");
	cr_redirect_stdout();
	epi_printd(tmp);
	epi_ddestroy(tmp);
	cr_expect_stdout_eq_str("J'AIMELESPONEY");
}
