/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** declaration file
*/

#pragma once
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "epi_struct.h"
#define ___true___ 1
#define ___false___ 0
#define DEPRECATED(var) __attribute__ ((deprecated(var)))

/**
* \file epilib.h
*/

/**
* \fn void epi_sort(char **tab);
* \brief sort a double array
* \param tab is the double array to be sorted
* \return void
*/
void epi_sort(char **tab);

/**
* \fn void epi_add_chained(chained_t *chained, int fd);
* \brief add a chained struct to the given chained list
* \param chained the chained list
* \param id the ID of the chained struct to add (-1 if you do not care)
* \return void
*/
void epi_add_chained(chained_t *chained, int id);

/**
* \fn void epi_add_dchained(chained_t *chained, int id);
* \brief add a double chained struct to the given double chained list
* \param chained the double chained list
* \param id the ID of the double chained struct to add (-1 if you do not care)
* \return void
*/
void epi_add_dchained(chained_t *chained, int id);

/**
* \fn void epi_destruct_chained(chained_t *chained);
* \brief destroy a chained or double chained list
* \param chained the chained list to destroy
* \return void
*/
void epi_destruct_chained(chained_t *chained);

/**
* \fn void epi_remove_chained(chained_t *chained, int id);
* \brief remove a chained struct in the given chained list
* \param chained the chained list
* \param id the ID of the chained struct to remove
* (remove nothing if id does not exist)
* \return void
*/
void epi_remove_chained(chained_t *chained, int id);

/**
* \fn int epi_count_words_words(char *str, char *sep);
* \brief count nomber of words in given string
* \param str the string to count words from
* \param sep the separator string
* \return int the number of words found
*/
int epi_count_words_words(char *, char *);

/**
* \fn int epi_atoi(char *str);
* \brief try to put str into an int ("123" -> 123)
* stop when it encounters a non numerical character
* \param str the string to put into number
* \return int the number obtained
*/
int epi_atoi(char *);

/**
* \fn int epi_putchar(unsigned char c);
* \brief print the given char
* \param c the char to display
* \return int 84 if write fails and 0 if it succeed
*/
int epi_putchar(unsigned char);

/**
* \fn int epi_putstr(char *str);
* \brief print the string given as argument
* \param str the string to display
* \return int 84 if write fails 0 if it succeed
*/
int epi_putstr(char *);

/**
* \fn int epi_puterr(char *str);
* \brief print the string given as argument on the error output
* \param str the string to display
* \return int 84 if write fails 0 if it succeed
*/
int epi_puterr(char *);

/**
* \fn int epi_strlen(char *str);
* \brief return the length of the string given
* \param str the string
* \return int the length of the string given (0 if str is NULL)
*/
int epi_strlen(char *);

/**
* \fn int epi_dstrlen(char **tab);
* \brief returns the length of the double array given
* \param tab the double array
* \return int the length of the double array given (0 if tab is NULL)
*/
int epi_dstrlen(char **);

/**
* \fn epi_start_with(char *str, char *start)
* \brief check if the string str starts with the start string
* \param str the original string
* \param start the string uses as start
* \return int -1 if not found, the startingposition if found
*/
int epi_start_with(char *, char *);

/**
* \fn int epi_end_with(char *str, char *end, int length);
* \brief check if the string end with a special string
* \param str the original string
* \param end the string uses as end
* \param length the integer from wich the start can begin
* \return int 0 if is not found, 1 if found
*/
int epi_end_with(char *, char *, int);

/**
* \fn int epi_contain_char(char *str, char c);
* \brief check if the string given contains a given character
* \param str the string to check
* \param c the char
* \return int 0 if not found, 1 if found
*/
int epi_contain_char(char *, char);

/**
* \fn int epi_contain_str(char *str, char *substr);
* \brief check if the string given contains another given string
* \param str the original string
* \param substr the substring
* \return int 0 if substr is not in str 1 if substr is in str
*/
int epi_contain_str(char *, char *);

/**
* \fn int epi_find(char **tab, char *str);
* \brief check if str is in tab
* \param tab the double array
* \param str the string to find
* \return int 0 if str is not in tab 1 if str is in tab
*/
int epi_find(char **, char *);

/**
* \fn int epi_ddestroy(char **tab);
* \brief destroy a double array completely malloc
* \param tab the double array to destroy
* \param
* \return int 84 if fails, 0 if succeed
*/
int epi_ddestroy(char **);

/**
* \fn int epi_printnbr_base_maj(long long number, int base);
* \brief print the iven number in the given base
* \param number the number to print
* \param base the base number (from 2 to 32)
* \return int always return 0
*/
int epi_printnbr_base_maj(long long, int);

/**
* \fn int epi_is_digit(char *str);
* \brief check if the given string can be converted into number
* \param str the string to check if it is composed of only number
* \return int returns 0 if not composed only of digits 1 else
*/
int epi_is_digit(char *);

/**
* \fn int epi_reverse_str(char *str);
* \brief reverse the string given as argument
* \param str the string to reverse
* \return int returns 0
*/
int epi_reverse_str(char *);

/**
* \fn int epi_replace_charac(char *str, char to_rep, char rep_by);
* \brief replace all characters to_rep in str by rep_by
* \param str the original string
* \param to_rep the character to replace
* \param rep_by the character used to replace to_rep
* \return int -1 if cannot replace 0 otherwise
*/
int epi_replace_charac(char *, char, char);

/**
* \fn int epi_nmatch(char const *str1, char const *str2);
* \brief check how much the second string can fit in the first one.
* a star can replace any substring
* \param str1 the original string
* \param str2 the substring
* \return int the number of substring found
*/
int epi_nmatch(char const *, char const *);

/**
* \fn int epi_printf(char *com, ...);
* \brief acts like printf with the following listed flags
* \note \%c requires a char and prints it
* \note \%s requires a string and prints it
* \note \%S requires a string and prints it but all non printable character
* are replaced with their octal value.
* \note \%i and \%d require an int and print it
* \warning \%i and \%d have undefined behaviour when used with negative number
* \warning This undefine behavior makes this function deprecated.
* please use epi_dprintf(1, com, ...) instead for now.
* \note \%f and \%F require a float and print it with 6
* \note \%o requires an int and print it in octal
* \note \%x requires an int and print it in low case hexadecimal
* \note \%x requires an int and print it in upper case hexadecimal
* \note \%p requires an int and print it in hexadecimal with 0x at start
* \note \%b requires an unsigned int and print it in binary
* \note \%u requires an unsigned int and print it
* \note \%#o requires an int and prints it in octal with 0 at start
* \note \%#x requires an int and prints it in hexadecimal with 0 or 0x at start
* \note \%#X same thing as %#x but in upper case
* \param com the string containing the flag list.
* \param ... all the others arguments.
* \return int always returns 0
*/
int epi_printf(char *, ...) DEPRECATED("use epi_dprintf(1, char *str, ...) \
instead. epi_printf will be replaces in future version.");

/**
* \fn int epi_printd(char **tab);
* \brief dump a given double array
* \param tab the double array to display
* \return int -1 if fails, 0 if succeed
*/
int epi_printd(char **);

/**
* \fn int epi_printnbr_base(long long number, int base);
* \brief print the given number in the given base in lower case (base 2->32)
* \param number the number to display
* \param the target base
* \return int always return 0
*/
int epi_printnbr_base(long long, int);

/**
* \fn int epi_putfloat(double number);
* \brief print the float given as argument with a precision of 6
* \param number the float to display
* \return int always return 0
*/
int epi_putfloat(double);

/**
* \fn int epi_putfloat_err(double number);
* \brief print the float given as argument with a precision of 6 on stderr
* \param number the float to display
* \return int always return 0
*/
int epi_putfloat_err(double);

/**
* \fn int epi_clear_malloc(char *str, int size);
* \brief clear the given string with \0
* \param str the string to clear
* \param size the size to clear
* \return int always return 0
*/
int epi_clear_malloc(char *, int);

/**
* \fn int epi_putnbr(int nb);
* \brief takes an integer and print it
* \param nb the number to display
* \return int always return 0
*/
int epi_putnbr(int);

/**
* \fn int epi_putnbr_err(int nb);
* \brief takes an integer and print it on stderr
* \param nb the number to display
* \return int always return 0
*/
int epi_putnbr_err(int);

/**
* \fn int epi_compare_str(char *str1, char *str2);
* \brief check if the two given string match.
* \note if both strings are NULL, count as a match
* \param str1 the first string
* \param str2 the second string
* \return int 0 if no match, 1 if match
*/
int epi_compare_str(char *, char *);

/**
* \fn int epi_count_words(char *str, char sep);
* \brief count all words with the given separator
* \param str the original string
* \param sep the separator of the words
* \return int the number of words found. If str is NULL returns 1
*/
int epi_count_words(char *, char separator);

/**
* \fn int epi_count_words_arg(char *str, char sep);
* \brief count all words with the given separator but ignore all things inside
* quotes and double quotes.
* \param str the original string
* \param sep the separator of the words
* \return int the number of words found. If str is NULL returns 1
*/
int epi_count_words_arg(char *, char separator);

/**
* \fn int epi_dprintf(int fd, char *str, ...);
* \brief acts like printf but you can choose the wanted fd to write to
* \note \%c requires a char and prints it
* \note \%s requires a string and prints it
* \note \%S requires a string and prints it but all non printable character
* are replaced with their octal value.
* \note \%i and \%d require an int and print it
* \note \%f and \%F require a float and print it with 6
* \note \%o requires an int and print it in octal
* \note \%x requires an int and print it in low case hexadecimal
* \note \%x requires an int and print it in upper case hexadecimal
* \note \%p requires an int and print it in hexadecimal with 0x at start
* \note \%b requires an unsigned int and print it in binary
* \note \%u requires an unsigned int and print it
* \note \%#o requires an int and prints it in octal with 0 at start
* \note \%#x requires an int and prints it in hexadecimal with 0 or 0x at start
* \note \%#X same thing as %#x but in upper case
* \param fd the file descriptor to write to
* \param str the string containing the flag list
* \return int 84 if an error occurred, 0 if it succeeded
*/
int epi_dprintf(int, char *, ...);

/**
* \fn int epi_free(void *ptr);
* \brief free the given pointer and add a NULL security
* \param ptr the pointer to free
* \return int 0 if succeed 1 else
*/
int epi_free(void *);

/**
* \fn int epi_free_many(void *ptr, ...);
* \brief free as many pointers as given, until the value 1 is reached
* \notes this functions calls epi_free() so it's NULL safe
* \param ptr the first pointer to free
* \param ... all the others pointers
* \return int always return 0
*/
int epi_free_many(void *, ...);

/**
* \fn long long epi_putnbr_dec(char *nb, int base);
* \brief takes a string in a given base and returns it in int
* \param nb the number to convert into an int
* \param base the base of the number given. can be taken from 2 to 36
* \warning the original number MUST be lower case
* \return long long the number in int
*/
long long epi_putnbr_dec(char *, int);

/**
* \fn long long epi_pow(int nb, int power);
* \brief takes a number ant put it to the power given
* \param nb the number to pow
* \param power the power wanted
* \return long long the result
*/
long long epi_pow(int, int);

/**
* \fn char *epi_fusion_str(char *str1, char *str2);
* \brief takes two string and concat them
* \warning no NULL safety.
* \param str1 the first string
* \param str2 the second string
* \return char * the obtained concat string
*/
char *epi_fusion_str(char *, char *);

/**
* \fn char *epi_fusion_str_many(char *str, ...);
* \brief takes as many string as given and concat them
* \warning stop at the first NULL string given
* \param str the first string
* \param ... all the strings to concat
* \return the concat result
*/
char *epi_fusion_str_many(char *, ...);

/**
* \fn char *epi_putnbr_base(long long nb, int base);
* \brief takes the given decimal number and returns it in the given base
* \param nb the given number
* \param base the target base
* \return char * the obtained number in lower case
*/
char *epi_putnbr_base(long long, int);

/**
* \fn char *epi_itoa(int nb);
* \brief takes the decimal number and put it into a string
* \param nb the number to convert to string
* \return char * the obtained string
*/
char *epi_itoa(int);

/**
* \fn char *epi_dtoa(double nb);
* \brief takes a double and convert it into string with a precision of 6
* \param nb the number to convert to string
* \return char * the obtained string
*/
char *epi_dtoa(double);

/**
* \fn char *epi_join(char **tab, char sep);
* \brief takes a double array and convert it into a string
* \param tab the double array
* \param sep the char that will be added between all the tabs string
* \return char * the obtained string
*/
char *epi_join(char **, char);

/**
* \fn char *epi_join_words(char **tab, char *sep);
* \brief same as epi_join but with a word as separator
* \param tab the double array
* \param sep the string that will be added between all the tabs string
* \return char * the obtained string
*/
char *epi_join_words(char **, char *);

/**
* \fn char *epi_malloc(int size);
* \brief allocate a char * with a given size and initialize it with \0
* \param size the size of the required allocation
* \return char * the obtained string
*/
char *epi_malloc(int);

/**
* \fn char *epi_malloc_spe(int size, char c);
* \brief same as epi_malloc but add the possibility to choose the initializer
* \param size the size of the required allocation
* \param c the initializer
* \return char * the obtained string
*/
char *epi_malloc_spe(int, char);

/**
* \fn char *epi_remalloc(char *str, int size);
* \brief copy a string with the given size (-1 to copy all)
* \param str the string to copy
* \param size the size of the reallocation wanted
* \return char * the copy
*/
char *epi_remalloc(char *, int);

/**
* \fn char *epi_stradd(char **str, char c);
* \brief takes the adress of a string
* \param str the adress of the string
* \param c the char to add to the string
* \return char * the old string so it can be free
*/
char *epi_stradd(char **, char);

/**
* \fn char *epi_get_word(char *str, char *sep1, char *sep2);
* \brief get the string contained between sep1 and sep2 in the string given
* \param str the original string
* \param sep1 the first separator
* \param sep2 the second separator
* \return char *the inside content (allocated)
*/
char *epi_get_word(char *, char *, char *);

/**
* \fn char *epi_lower_str(char *str);
* \brief takes a string and put every letters into lowercase
* \param str the original string
* \param
* \return char * the string in lower case
*/
char *epi_lower_str(char *);

/**
* \fn char *epi_remove_charac(char **str, char c);
* \brief takes the adress of a string and remove all characters given from it
* \param str the adress of the original string
* \param c the character to be removed
* \return char * the old string so it can be free
*/
char *epi_remove_charac(char **, char);

/**
* \fn char *epi_putnbr_base_maj(long long nb, int base);
* \brief takes a number and returns it in the given base
* \param nb the number in decimal
* \param base the target base
* \return char * the obtained number
*/
char *epi_putnbr_base_maj(long long, int);

/**
* \fn char *epi_base_to_base(char *nb, int base1, int base2, int is_maj)
* \brief takes a number from a certain base and convert it into another base
* \param nb the original number
* \param base1 the original base
* \param base2 the target base
* \param is_maj inform if the original number is upper or lower case
* \return char * the obtained number
*/
char *epi_base_to_base(char *, int, int, int);

/**
* \fn char *epi_putf(char *str, ...);
* \brief works as printf but returns the string obtained instead of printing it
* \note \%c requires a char and put it into the string
* \note \%s requires a string and put it into the string
* \note \%S requires a string and put it into a string but all non printable
* character are replaced with their octal value.
* \note \%i and \%d require an int and put it into the string
* \note \%f and \%F require a float and put it into the string with a precision
* of 6
* \note \%o requires an int and put it into the string in octal
* \note \%x requires an int and put it into the string in low case hexadecimal
* \note \%x requires an int and put it into the string in upper case
* hexadecimal
* \note \%p requires an int and put it into a string in hexadecimal with 0x
* at start
* \note \%b requires an unsigned int and put it into the string in binary
* \note \%u requires an unsigned int and put it into the string
* \note \%#o requires an int and put it into the string in octal with 0 at
* start
* \note \%#x requires an int and put it into the string in hexadecimal with 0
* or 0x at start
* \note \%#X same thing as %#x but in upper case
* \param str the flag string
* \param ... all the flags arguments
* \return char * the string obtained
*/
char *epi_putf(char *, ...);

/**
* \fn char *epi_vprintf(char *str, va_list list);
* \brief works as printf but returns the string obtained instead of printing it
* \note \%c requires a char and put it into the string
* \note \%s requires a string and put it into the string
* \note \%S requires a string and put it into a string but all non printable
* character are replaced with their octal value.
* \note \%i and \%d require an int and put it into the string
* \note \%f and \%F require a float and put it into the string with a precision
* of 6
* \note \%o requires an int and put it into the string in octal
* \note \%x requires an int and put it into the string in low case hexadecimal
* \note \%x requires an int and put it into the string in upper case
* hexadecimal
* \note \%p requires an int and put it into a string in hexadecimal with 0x
* at start
* \note \%b requires an unsigned int and put it into the string in binary
* \note \%u requires an unsigned int and put it into the string
* \note \%#o requires an int and put it into the string in octal with 0 at
* start
* \note \%#x requires an int and put it into the string in hexadecimal with 0
* or 0x at start
* \note \%#X same thing as %#x but in upper case
* \param str the flag string
* \param list all the flags arguments in a va_list
* \return char * the string obtained
*/
char *epi_vprintf(char *, va_list);

/**
* \fn char **epi_split(char *str, char sep);
* \brief split a string with the given separator
* \param str the original string
* \param sep the separation character
* \return char ** the double array obtained
*/
char **epi_split(char *, char);

/**
* \fn char **epi_split_light(char *str, char sep, int does_remalloc)
* \brief split a string with the given separator in a lightweight version
* \param str the original string
* \warning The original string cannot be used after the split!
* \param sep the separation character
* \param does_remalloc if 1 indicate not to realloc the string
* \return char ** the double array obtained
*/
char **epi_split_light(char *, char, int);

/**
* \fn char **epi_split_arg(char *str, char sep);
* \brief split a string with the given separator ignoring everything between
* quotes and double quotes
* \param str the original string
* \param sep the separation character
* \return char ** the double array obtained
*/
char **epi_split_arg(char *, char);

/**
* \fn char **epi_split_nb(char *str, int size, char *sep);
* \brief split a string with the given separator with a sizemax for each split
* \brief if one word exceed the size, ignore the size for this word
* \param str the original string
* \param size the size of one line
* \param sep the separation string
* \return char ** the double array obtained
*/
char **epi_split_nb(char *, int, char *);

/**
* \fn char **epi_split_words(char *str, char *sep);
* \brief split a string with the given separator
* \param str the original string
* \param sep the separation string
* \return char ** the double array obtained
*/
char **epi_split_words(char *, char *);

/**
* \fn char **epi_split_words(char *str, char *sep, int *i);
* \brief split a string with the given separator and check if two separators
* \brief are side by side
* \param str the original string
* \param sep the separation string
* \param i the adress of an int that goes to -1 if two separators are attached
* \warning i is not modified if the two separators are not attached
* \return char ** the double array obtained
*/
char **epi_split_facade(char *, char *, int *);

/**
* \fn char **epi_split_many(char *str, char *sep);
* \brief acts live epi_split() but with a list of separators
* \param str the original string
* \param sep the list of separators
* \return char ** the double array obtained
*/
char **epi_split_many(char *, char *);

/**
* \fn char **epi_split_many_arg(char *str, char *sep);
* \brief acts live epi_split_many but ignoring everything between quotes or
* double quotes
* \param str the original string
* \param sep the list of separators
* \return char ** the double array obtained
*/
char **epi_split_many_arg(char *, char *);

/**
* \fn char **epi_get_words(char *str, char c, int always_zero);
* \brief I have no idea what this function is or if it still exists.
* \warning If you find it, please tell me where it is
*/
char **epi_get_words(char *, char, int always_zero);

/**
* \fn char **epi_dre_malloc(char **tab);
* \brief takes a double array and copy it
* \param tab the double array to copy
* \return char ** the copy created
*/
char **epi_dre_malloc(char **);

/**
* \fn char **epi_dadd(char **tab, char *str);
* \brief takes a double array and add to it the string str at the end
* \param tab the original double array
* \param str the string to be added
* \return the new double array
*/
char **epi_dadd(char **, char *);

/**
* \fn char **epi_dadd_start(char **tab, char *str);
* \brief takes a double array and add to it the string str at the start
* \param tab the original double array
* \param str the string to be added
* \return the new double array
*/
char **epi_dadd_start(char **, char *);

/**
* \fn char **epi_remove_line(char ***tab, char *str);
* \brief takes the adress of a double array and remove all lines matching
* the given string
* \param tab the adress of the double array
* \param str the pattern that will be removed
* \return char ** the old array so it can be free
*/
char **epi_remove_line(char ***, char *);

/**
* \fn char **epi_remove_line_nb(char ***tab, int nb);
* \brief same as epi_remove_line() but an index is given
* \param tab the adress of the array
* \param nb the line number of the sting to be removed
* \return char ** the old array so it can be free
*/
char **epi_remove_line_nb(char ***, int);

/**
* \fn chained_t *epi_create_chained(int size);
* \brief create a chained list of a given size
* \param size the size of the list wanted
* \return chained_t * the chained list obtained
*/
chained_t *epi_create_chained(int);

/**
* \fn chained_t *epi_create_dchained(int size);
* \brief create a double chained list of a given size
* \param size the size of the wanted list
* \return chained_t *the double chained list obtained
*/
chained_t *epi_create_dchained(int);

/**
* \fn object_t new_obj(void *inside, char *type);
* \brief create an object and store the given pointer inside
* \param inside the pointer to store
* \param type the type (a string) to help the different internal function
* to work
* \return object_t the new object created
*/
object_t new_obj(void *, char *);

/**
* \fn epi_args_t *epi_get_args(int argc, char **argv);
* \brief split the arguments starting by '-' and the ones by "--" and store
* them separately to makes it easier to access them
* \param argc the length of argv
* \param argv the list of the program's arguments
* \return epi_args_t * the structure containing everything
*/
epi_args_t *epi_get_args(int, char **);
