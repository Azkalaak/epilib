/*
** EPITECH PROJECT, 2018
** the structure file
** File description:
** creating texture
*/

#pragma once

/**
* \file epi_struct.h
*/

/**
* \struct epi_args_t
* \brief structure containing all arguments of a double array
* (typedef of epi_args_s)
*/
typedef struct epi_args_s epi_args_t;
/**
* \struct chained_t
* \brief generical chained list implementation (typedef of chained_s)
*/
typedef struct chained_s chained_t;
/**
* \struct object_t
* \brief Structure for C object (already a pointer of object_s)
*/
typedef struct object_s *object_t;

struct object_s {
    void *inside;
    char *type;
    char *original_type;
    void *(*get_value)(object_t);
    int (*len)(object_t);
    void (*print)(object_t);
    void (*to_string)(object_t);
    void (*to_int)(object_t);
    void (*to_origin)(object_t);
    void (*supported_types)(void);
    void (*credit)(void);
    void (*destroy)(object_t);
    char *(*get_type)(object_t);
};

struct chained_s {
    void *in;
    int id;
    chained_t *next;
    chained_t *prev;
};

struct epi_args_s {
    char **argv;
    char *small_args;
    char **big_args;
};
