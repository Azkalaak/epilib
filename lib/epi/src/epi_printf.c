/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** 2 month later :D
*/

#include "../internal.h"

/*
**INTERNAL LIB FUNCTION DO NOT USE IT /!\
**internal function of epi_printf, in case a '%'
*/
void internal_switch_manage4(va_list list, char *str, int *i)
{
    switch (str[*i]) {
    case '#':
        *i += 1;
        internal_hash_gest(list, str, i);
        break;
    case 'S':
        internal_putstr_o(va_arg(list, char *));
        break;
    default:
        internal_switch_manage(list, str[*i]);
    }
}

/*
**INTERNAL LIB FUNCTION DO NOT USE IT /!\
**internal function of epi_printf, in case a '%'
*/
void internal_switch_manage3(va_list list, char c)
{
    switch (c) {
    case 'c':
        epi_putchar((unsigned char)va_arg(list, int));
        break;
    case 'b':
        epi_printnbr_base(va_arg(list, unsigned int), 2);
        break;
    case 'p':
        epi_putstr("0x");
        epi_printnbr_base(va_arg(list, long long), 16);
        break;
    default:
        epi_dprintf(1, "error: flag '%%%c' not found.\n", c);
    }
}

/*
**INTERNAL LIB FUNCTION DO NOT USE IT /!\
**internal function of epi_printf, in case a '%'
*/
void internal_switch_manage2(va_list list, char c, unsigned int i)
{
    switch (c) {
    case 'o':
        epi_printnbr_base(va_arg(list, long long), 8);
        break;
    case 'u':
        i = va_arg(list, unsigned int);
        epi_printnbr_base(i, 10);
        break;
    case 'x':
        epi_printnbr_base((long long)va_arg(list, int), 16);
        break;
    case 'X':
        epi_printnbr_base_maj((long long)va_arg(list, int), 16);
        break;
    default:
        internal_switch_manage3(list, c);
    }
}

/*
**INTERNAL LIB FUNCTION DO NOT USE IT /!\
**internal function of epi_printf, in case a '%'
*/
void internal_switch_manage(va_list list, char c)
{
    switch (c) {
    case 's':
        epi_putstr(va_arg(list, char *));
        break;
    case 'i':
    case 'd':
        epi_printnbr_base(va_arg(list, int), 10);
        break;
    case 'F':
    case 'f':
        epi_putfloat(va_arg(list, double));
        break;
    case '%':
        epi_putstr("%");
        break;
    default:
        internal_switch_manage2(list, c, 0);
    }
}

/*
**OUTDATED FUNCTION WILL BE REPLACED IN NEXT LIB GENERATION
**read du_de_le_man to know how to use it (if you don't have acess to it
**read the manual of the non float flags of printf)
*/
int epi_printf(char *str, ...)
{
    va_list list;

    va_start(list, str);
    if (str == NULL) {
        epi_putstr(NULL);
        return (0);
    }
    for (int i = 0; str[i]; ++i) {
        if (str[i] == '%') {
            ++i;
            internal_switch_manage4(list, str, &i);
        } else if (write(1, &str[i], 1) != -1)
            continue;
    }
    va_end(list);
    return (0);
}
