/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** epi_free
*/

#include "../internal.h"

/*
**takes void pointer as argument and try to free them until it found a pointer
**equal to 1
**DO NOT FORGET TO SEND A '1' AT THE END OR IT WILL CAUSE A SEGFAULT
*/
int epi_free_many(void *str, ...)
{
    va_list list;

    va_start(list, str);
    while (1) {
        if (str == (void *)1)
            break;
        epi_free(str);
        str = va_arg(list, void *);
    }
    va_end(list);
    return (0);
}

/*
**takes a void pointer as argument and try to free it
**if the pointer given is NULL returns 1 else returns 0
*/
int epi_free(void *ptr)
{
    if (!ptr)
        return (1);
    free(ptr);
    return (0);
}
