/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** re_malloc
*/

#include "../internal.h"

/*
**takes a string as argument and malloc another string of size nb and fill
**it with the characters contained in the first nb characters of the first one.
**if nb is negative, the size is equal to the size of the string given as
**argument. returns the result
*/
char *epi_remalloc(char *str, int nb)
{
    char *sol;
    int nbis = epi_strlen(str);

    if (nb < 0)
        nb = nbis;
    sol = epi_malloc(nb);
    for (int i = 0; i < nb; ++i)
        sol[i] = str[i];
    return (sol);
}
