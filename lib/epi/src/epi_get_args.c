/*
** EPITECH PROJECT, 2018
** lib 3.0
** File description:
** epi_get_args
*/

#include "../internal.h"

epi_args_t *epi_get_args(int argc, char *argv[])
{
    epi_args_t *arguments = malloc(sizeof(epi_args_t));

    if (!arguments)
        return (NULL);
    arguments->argv = NULL;
    arguments->small_args = NULL;
    arguments->big_args = NULL;
    for (int i = 0; i < argc; ++i) {
        if (i != 0 && epi_start_with(argv[i], "--") != -1) {
            arguments->big_args = epi_dadd(arguments->big_args, &argv[i][2]);
            continue;
        } else if (i != 0 && epi_start_with(argv[i], "-") != -1);
        else {
            arguments->argv = epi_dadd(arguments->argv, argv[i]);
            continue;
        }
        for (int j = 1; argv[i][j];
        epi_free(epi_stradd(&arguments->small_args, argv[i][j])), ++j);
    }
    return (arguments);
}
