/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** replace all charac c in str
*/

#include "../internal.h"

/*
**tahes a string as parameter and check if it is composed only of digits
**returns 0 if not and 1 if yes
*/
int epi_is_digit(char *str)
{
    for (int i = 0; str[i]; ++i) {
        if (str[i] == '-' || str[i] == '+')
            continue;
        if (str[i] < 48 || str[i] > 57)
            return (0);
    }
    return (1);
}

/*
**tahes a string and returns it's content.
*/
int epi_reverse_str(char *str)
{
    int nb = epi_strlen(str) - 1;
    char c = 0;

    for (int i = 0; i < nb; ++i, --nb) {
        c = str[i];
        str[i] = str[nb];
        str[nb] = c;
    }
    return (0);
}

/*
**takes a string and returns the same string, except that all characters
**that are the same as the one given are removed (cannot remove '\0')
*/
char *epi_remove_charac(char **str, char char_to_remove)
{
    int new_length = 0;
    char *temp = *str;

    if (char_to_remove == 0) {
        epi_dprintf(1, "error, \0 cannot be remove\n");
        return (NULL);
    }
    for (int i = 0; temp[i]; ++i)
        if (temp[i] != char_to_remove)
            ++new_length;
    if (new_length == 0)
        new_length = 1;
    *str = epi_malloc_spe(new_length, 0);
    new_length = 0;
    for (int i = 0; temp[i]; ++i)
        if (temp[i] != char_to_remove) {
            str[0][new_length] = temp[i];
            ++new_length;
        }
    return (temp);
}

/*
**takes a string as argument and replace every charac_to_replace by
**charac_replaced
**'\0' cannot be replaced
*/
int epi_replace_charac(char *str, char charac_to_replace, char charac_replaced)
{
    if (charac_to_replace == 0) {
        epi_dprintf(1, "error, \0 cannot be replaced\n");
        return (-1);
    }
    for (int i = 0; str[i]; ++i) {
        if (str[i] == charac_to_replace)
            str[i] = charac_replaced;
    }
    return (0);
}
