/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** epi_wordtoarray
*/

#include "../internal.h"

/*
**takes a double array as argument and returns a fusion of all line
**separate by the word given as argument
*/
char *epi_join_words(char **str, char *word)
{
    int nb = epi_dstrlen(str);
    char *sol = NULL;
    char *temp = sol;

    if (nb <= 1)
        return (epi_remalloc(str[0], -1));
    sol =epi_fusion_str_many(str[0], NULL);
    temp = sol;
    for (int i = 1; i < nb; ++i) {
        sol =epi_fusion_str_many(sol, word, str[i], NULL);
        epi_free(temp);
        temp = sol;
    }
    return (sol);
}

/*
**takes a double array as argument and returns a fusion of all line
**separate by the separate given as argument
*/
char *epi_join(char **str, char separator)
{
    int nb = epi_dstrlen(str);
    int length = 0;
    int count = 0;
    char *sol;

    for (int i = 0; nb > i; ++i)
        length += epi_strlen(str[i]);
    length += nb - 1;
    sol = epi_malloc(length);
    for (length = 0; str[length]; ++length) {
        for (int i = 0; str[length][i]; ++i, ++count)
            sol[count] = str[length][i];
        if (str[length + 1] != NULL) {
            sol[count] = separator;
            ++count;
        }
    }
    return (sol);
}

/*
**INTERNAL LIB FUNCTION DO NOT USE /!\
*/
int internal_get_argument(char *sol, int *k, int *i, char *str)
{
    char c = str[*i];

    if (c != 34 && c != 39) {
        sol[*k] = str[*i];
        *i += 1;
        *k += 1;
        return (0);
    }
    for (int j = 0; str[*i - 1] != c || j < 2; *k += 1, *i += 1, ++j) {
        if (str[*i] == 0)
            break;
        sol[*k] = str[*i];
    }
    return (0);
}

/*
**split the string given as argument with a separator given ignoring the ones
**between quotes
**returns the double array obtained
*/
char **epi_split_arg(char *str, char separator)
{
    int word_nb = epi_count_words_arg(str, separator);
    int len = epi_strlen(str);
    char **sol = malloc(sizeof(char *) * (word_nb + 1));
    int i = 0;

    if (sol == NULL || str == NULL)
        return (NULL);
    sol[word_nb] = NULL;
    for (int i = 0; i < word_nb; ++i)
        sol[i] = epi_malloc_spe(len, 127);
    for (int j = 0; sol[j]; ++j) {
        for (; str[i] == separator && str[i]; ++i);
        for (int k = 0; str[i] != separator && str[i] != 0;)
            internal_get_argument(sol[j], &k, &i, str);
        free(epi_remove_charac(&sol[j], 127));
    }
    return (sol);
}

/*
**split the string given as argument with a separator given
**returns the double array obtained
*/
char **epi_split(char *str, char separator)
{
    int word_nb = epi_count_words(str, separator);
    int len = epi_strlen(str);
    char **sol = malloc(sizeof(char *) * (word_nb + 1));
    int i = 0;

    if (sol == NULL || sol == NULL)
        return (NULL);
    sol[word_nb] = NULL;
    for (int i = 0; i < word_nb; ++i)
        sol[i] = epi_malloc_spe(len, 127);
    for (int j = 0; sol[j]; ++j) {
        for (; str[i] == separator && str[i] != 0; ++i);
        for (int k = 0; str[i] != separator && str[i] != 0; ++i, ++k)
            sol[j][k] = str[i];
        free(epi_remove_charac(&sol[j], 127));
    }
    return (sol);
}
