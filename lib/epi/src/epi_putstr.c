/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** epi_putstr
*/

#include "../internal.h"

/*
**print the character given on the standard output
*/
int epi_putchar(unsigned char c)
{
    if (write(1, &c, 1) == -1)
        return 84;
    return 0;
}

/*
**print the string given on the standard output
**if string is NULL, print (NULL)
*/
int epi_putstr(char *str)
{
    int i = 0;

    if (str == NULL) {
        i = write(1, "(NULL)", 6);
        return (84);
    }
    i = write(1, str, epi_strlen(str));
    if (i == -1)
        return 84;
    return (0);
}
