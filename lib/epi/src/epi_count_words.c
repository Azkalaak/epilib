/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** my count words
*/

#include "../internal.h"

/*
**INTERNAL LIB FUNCTION USE IT AT YOUR OWN RISKS
*/
void internal_skip_word(int *i, char *str, char quote)
{
    *i += 1;
    for (; str[*i] != quote; *i += 1)
        if (str[*i] == 0)
            break;
}

/*
**INTERNAL LIB FUNCTION USE IT AT YOUR OWN RISKS
*/
int internal_count_words_words2(char *str, char *splitters, int word_nb)
{
    int temp = 0;

    for (int i = 0; str[i]; ++i) {
        temp = epi_start_with(&str[i], splitters);
        if (temp == -1)
            continue;
        i += temp;
        temp = epi_start_with(&str[i], splitters);
        for (;temp != -1 && str[i] != 0;) {
            i += temp;
            temp = epi_start_with(&str[i], splitters);
        }
        if (str[i])
            ++word_nb;
    }
    return (word_nb);
}

/*
**INTERNAL LIB FUNCTION USE IT AT YOUR OWN RISKS
**count words in str separated by the string splitter
*/
int epi_count_words_words(char *str, char *splitters)
{
    int word_nb = 0;
    int temp = epi_start_with(str, splitters);

    if (str == NULL || splitters == NULL)
        return (1);
    if (temp == -1 && str[0]) {
        ++word_nb;
        temp = 0;
    }
    word_nb = internal_count_words_words2(str, splitters, word_nb);
    return (word_nb);
}

/*
**INTERNAL LIB FUNCTION USE IT AT YOUR OWN RISKS
**count words in str separated by the charac space, ignoring the quotes and
**double quotes
*/
int epi_count_words_arg(char *str, char space)
{
    int word_nb = 0;

    if (str == NULL)
        return (1);
    if (str[0] != space && str[0] != 0)
        ++word_nb;
    for (int i = 0; str[i]; ++i) {
        if (str[i] == 34)
            internal_skip_word(&i, str, 34);
        if (str[i] == 39)
            internal_skip_word(&i, str, 39);
        if (str[i] == space && str[i + 1] &&
            str[i + 1] != space)
            ++word_nb;
    }
    return (word_nb);
}

/*
**INTERNAL LIB FUNCTION USE IT AT YOUR OWN RISKS
**count words in str separated by the charac space
*/
int epi_count_words(char *str, char space)
{
    int word_nb = 0;

    if (str == NULL)
        return (1);
    if (str[0] != space && str[0] != 0)
        ++word_nb;
    for (int i = 0; str[i]; ++i) {
        if (str[i] == space && str[i + 1] != space &&
            str[i + 1])
            ++word_nb;
    }
    return (word_nb);
}
