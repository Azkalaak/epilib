/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** nbr to decimal
*/

#include "../internal.h"

/*
**takes a number and returns it at the power given
**NO OVERFLOW SECURITY!!!
*/
long long epi_pow(int nb, int power)
{
    long long tempo = (long long)nb;

    if (power == 0)
        return (1);
    for (int i = 0; i < power; ++i)
        tempo = tempo * nb;
    return (tempo);
}

/*
**find the car 'c' in the array given and returns it's position
**if the character is not found, returns -1
*/
int epi_find_in_array(char *str, char c)
{
    for (int i = 0; str[i]; ++i)
        if (str[i] == c)
            return (i);
    return (-1);
}

/*
**takes a string containing numbers in a base given and convert it
**into the decimal base, then return the result
*/
long long epi_putnbr_dec(char *nbr, int base_nb)
{
    int len = epi_strlen(nbr) - 1;
    char *base = "0123456789abcdefghijklmnopqrstuvwxyz";
    int power = 1;
    char *str = epi_lower_str(nbr);
    long long sol = 0;

    if (base_nb > 36)
        base_nb = 36;
    if (base_nb < 2)
        base_nb = 2;
    if (len > 128)
        return (0);
    for (int i = len; i >= 0; --i) {
        sol += epi_find_in_array(base, str[i]) * power;
        power = power * base_nb;
    }
    epi_free(str);
    return (sol);
}
