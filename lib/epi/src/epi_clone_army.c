/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** copying double array
*/

#include "../internal.h"

/*
**this function takes a double array (finishing by '\0') and return it's size
**returns 0 if the double array is NULL
*/
int epi_dstrlen(char *str[])
{
    int i = 0;

    if (!str)
        return (0);
    for (i = 0; str[i] != NULL; ++i);
    return (i);
}

/*
**destroy the double array given as argument
**return 84 if the double array does not exist
**return 0 if it does
*/
int epi_ddestroy(char *str[])
{
    if (str == NULL)
        return (84);
    for (int i = 0; str[i] != NULL; ++i) {
        epi_free(str[i]);
        str[i] = NULL;
    }
    epi_free(str);
    return (0);
}

/*
**print the entire double array given as argument without separator
**if the double array is NULL it returns -1
**else, return 0
*/
int epi_printd(char **str)
{
    if (str == NULL)
        return (-1);
    for (int i = 0; str[i] != NULL; ++i) {
        epi_putstr(str[i]);
    }
    return (0);
}

/*
**copy the double array given as argument and return the copy
**if the double array given is NULL returns a double array with NULL as
**first argument.
*/
char **epi_dre_malloc(char *str[])
{
    int len = epi_dstrlen(str);
    char **sol = malloc(sizeof(char *) * (len + 1));

    sol[len] = NULL;
    for (int i = 0; str[i]; ++i) {
        sol[i] = epi_remalloc(str[i], -1);
    }
    return (sol);
}
