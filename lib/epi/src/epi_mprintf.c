/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** all printf mains
*/

#include "../internal.h"

/*
**works like epi_printf but returns the string instead of printing it
*/
char *epi_putf(char *str, ...)
{
    va_list list;
    char *sol = NULL;

    if (str == NULL)
        return (NULL);
    va_start(list, str);
    sol = epi_vprintf(str, list);
    va_end(list);
    return (sol);
}

/*
**works like epi_printf but you can choose the output of the print
**(1 for the standard one, 2 for the error one a file descriptor for a file)
*/
int epi_dprintf(int fd, char *str, ...)
{
    va_list list;
    char *sol = NULL;

    va_start(list, str);
    if (fd < 0 || str == NULL)
        return (84);
    sol = epi_vprintf(str, list);
    if (write(fd, sol, epi_strlen(sol)) == -1)
        return 84;
    free(sol);
    va_end(list);
    return (0);
}
