/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** putnbr_base
*/

#include "../internal.h"

/*
**takes a decimal number and print it in the base given (letter in MAJ)
**max base: base 32
*/
int epi_printnbr_base_maj(long long nb, int base_nb)
{
    char *str = epi_putnbr_base_maj(nb, base_nb);

    epi_putstr(str);
    free(str);
    return (0);
}

/*
**takes a decimal number and print it in the base given (letter in MIN)
**max base: base 32
*/
int epi_printnbr_base(long long nb, int base_nb)
{
    char *str = epi_putnbr_base(nb, base_nb);

    epi_putstr(str);
    free(str);
    return (0);
}

/*
**takes a decimal number and returns it in the base given (letter in MAJ)
**max base: base 32
*/
char *epi_putnbr_base_maj(long long nb, int base_nb)
{
    char *base = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *str = epi_malloc_spe(128, 127);

    if (base_nb > epi_strlen(base))
        base_nb = epi_strlen(base);
    if (base_nb < 2)
        base_nb = 2;
    if (nb == 0)
        str[0] = '0';
    for (int i = 0; nb >= 1; ++i) {
        str[i] = base[nb % base_nb];
        nb = nb / base_nb;
    }
    epi_replace_charac(str, 127, '\0');
    epi_reverse_str(str);
    return (str);
}

/*
**takes a decimal number and print it in the base given (letter in MIN)
**max base: base 32
*/
char *epi_putnbr_base(long long nb, int base_nb)
{
    char *base = "0123456789abcdefghijklmnopqrstuvwxyz";
    char *str = epi_malloc_spe(128, 127);

    if (base_nb > epi_strlen(base))
        base_nb = epi_strlen(base);
    if (base_nb < 2)
        base_nb = 2;
    if (nb == 0)
        str[0] = '0';
    for (int i = 0; nb >= 1; ++i) {
        str[i] = base[nb % base_nb];
        nb = nb / base_nb;
    }
    epi_replace_charac(str, 127, '\0');
    epi_reverse_str(str);
    return (str);
}
