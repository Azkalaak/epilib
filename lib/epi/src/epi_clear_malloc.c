/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** clear malloc
*/

#include "../internal.h"

/*
**this function takes a string and its size and replace every character
**on this string by '\0'
*/
int epi_clear_malloc(char *str, int size)
{
    for (int i = 0; i < size; ++i)
        str[i] = 0;
    return (0);
}
