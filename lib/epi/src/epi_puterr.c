/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** epi_puterr
*/

#include "../internal.h"

/*
**print the string str on the error output
*/
int epi_puterr(char *str)
{
    int i = 0;

    if (!str) {
        i = write(2, "(NULL)\n", 7);
        return 84;
    }
    if (write(2, str, epi_strlen(str)) == -1)
        return 84;
    return 0;
    (void)i;
}
