/*
** EPITECH PROJECT, 2018
** lib3.0
** File description:
** does contain
*/

#include "../internal.h"

int epi_contain_char(char *str, char c)
{
    if (!str)
        return (0);
    for (int i = 0; str[i]; ++i)
        if (str[i] == c)
            return (1);
    return (0);
}

int epi_find(char **tab, char *str)
{
    if (!tab || !str)
        return 0;
    for (int i = 0; tab[i]; ++i)
        if (epi_compare_str(tab[i], str) == 1)
            return 1;
    return 0;
}

int epi_contain_str(char *str1, char *str2)
{
    int save = 0;
    char is_good = 'n';
    char is_bad = 'n';

    if (!str1 || !str2)
        return (0);
    for (int i = 0; str1[i]; ++i) {
        save = i;
        for (int j = 0; is_bad == 'n' && is_good == 'n' &&
        str1[save] && str2[j]; ++j, ++save) {
            is_good = str2[j + 1] == 0 ? 'y' : 'n';
            is_bad = str2[j] != str1[save] ? 'y' : 'n';
        }
        if (is_good == 'y' && is_bad == 'n') {
            return (1);
        }
        is_bad = 'n';
        is_good = 'n';
    }
    return (0);
}
