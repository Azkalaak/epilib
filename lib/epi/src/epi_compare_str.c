/*
** EPITECH PROJECT, 2018
** mysh
** File description:
** compare str
*/

#include "../internal.h"

/*
**takes 2 strings as arguments
**returns 0 if the string does not match
**returns 1 if the strings are the same
**if both strings are NULL, return 1
*/
int epi_compare_str(char *str, char *str2)
{
    int i = 0;

    if ((!str && str2) || (str && !str2))
        return (___false___);
    if (!str && !str2)
        return (___true___);
    while (1) {
        if (str[i] != str2[i])
            return (___false___);
        if (str2[i] == 0 && str[i] == 0)
            return (___true___);
        if (str2[i] == 0 || str[i] == 0)
            return (___false___);
        ++i;
    }
    return (___true___);
}
