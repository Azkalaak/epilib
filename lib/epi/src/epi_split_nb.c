/*
** EPITECH PROJECT, 2018
** lib 2.0
** File description:
** splitting with number
*/

#include "../internal.h"

/*
**INTERNAL LIB FUNCTION DO NOT USE /!\
*/
char *internal_get_first_words(char *str[], int nb, int *len)
{
    char *sol =epi_fusion_str_many(str[0], NULL);
    char *temp = sol;

    *len = *len + 1;
    if (epi_strlen(sol) >= nb)
        return (sol);
    for (int i = 1; str[i]; ++i) {
        sol =epi_fusion_str_many(sol, " ", str[i], NULL);
        if (epi_strlen(sol) > nb) {
            epi_free(sol);
            return (temp);
        }
        if (temp) {
            epi_free(temp);
            temp = NULL;
        }
        temp = sol;
        *len = *len + 1;
    }
    return (sol);
}

/*
**split the string given as argument and returns a double array created
**as no line can contain more charac than the number given
*/
char **epi_split_nb(char *init, int nb, char *word_splitter)
{
    char **sol = NULL;
    char **str = epi_split_many(init, word_splitter);
    char *strbis = NULL;
    int to_remove = 0;

    for (; str[0];) {
        strbis = internal_get_first_words(str, nb, &to_remove);
        sol = epi_dadd(sol, strbis);
        if (strbis) {
            epi_free(strbis);
            strbis = NULL;
        }
        for (; to_remove > 0 && str && str[0]; --to_remove)
            epi_ddestroy(epi_remove_line_nb(&str, 0));
    }
    epi_ddestroy(str);
    return (sol);
}
