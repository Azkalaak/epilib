/*
** EPITECH PROJECT, 2017
** match!
** File description:
** hello <3
*/

#include "../internal.h"

/*
**see how many times s2 match in s1
**s2 can be composed of stars (they replace any string)
**WARNING, IF S1 IS TOO BIG IT WILL CAUSE A STACK OVERFLOW
*/
int epi_nmatch(char const *s1, char const *s2)
{
    if (*s1 == 0 && *s2 == 0)
        return (1);
    if (*s2 == '*' && *s1 != 0)
        return (epi_nmatch((s1 + 1), s2) + epi_nmatch(s1, (s2 + 1)));
    if (*s2 == '*' && *s1 == 0)
        return (epi_nmatch(s1, (s2 + 1)));
    if (*s1 == *s2)
        return (epi_nmatch((s1 + 1), (s2 + 1)));
    return (0);
}
