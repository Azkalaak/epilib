/*
** EPITECH PROJECT, 2018
** lib2.0
** File description:
** the internal lib function
*/

#pragma once

#include "epilib.h"

void internal_hash_gest(va_list, char *, int *);
void internal_putstr_o(char *);
void internal_putchar_o(char);
void internal_switch_manage(va_list, char);
void new_obj2(object_t);
void obj_print(object_t);
void obj_to_int(object_t);
void obj_destroy(object_t);
void obj_to_string(object_t);
void obj_to_origin(object_t);
void obj_supported_types(void);
void credit(void);
void *obj_get_value(object_t);
int obj_len(object_t);
int internal_length_nb(int);
char *obj_get_type(object_t);
char *internal_hashtag_gest(va_list, char *, int *);
char *internal_getchar_o(char);
char *internal_getstr_o(char *);
char *internal_switch1(va_list, char *, int *);
